<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Student;

class StudentController extends Controller
{
    public function actionView($id)
    {
        $model = new Student();
		$name = $model->getName($id);
		return $this->render('showOne' , ['name' => $name]);
    }
}


